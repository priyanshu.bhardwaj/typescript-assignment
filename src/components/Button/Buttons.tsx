import React from "react";
import ButtonComponent from "./ButtonComponent";

type ButtonsProps = {
  isActive: boolean,
  startTime:() => void ,
  stopTime:()=>void ,
  createLap:()=>void ,
  reset: ()=>void
}

const Buttons = ({
  isActive,
  startTime,
  stopTime,
  createLap,
  reset,
}: ButtonsProps) => {
  return (
    <div className="flex gap-5 mt-5">
      <ButtonComponent
        title={isActive ? "Lap" : "Reset"}
        createLap={createLap}
        reset={reset}
      />
      <ButtonComponent
        title={isActive ? "Stop" : "Start"}
        startTime={startTime}
        stopTime={stopTime}
      />
    </div>
  );
};

export default Buttons;
